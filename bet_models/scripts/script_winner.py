import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pandas import DataFrame
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler

from bet_models.Utils.model import SequentialBackwardSelection

data = pd.read_csv('../data/full.csv', parse_dates=['Date'], index_col=['Date'])
class_le = LabelEncoder()
data = data[["Div",  "HomeTeam", "AwayTeam", "FTHG", "FTAG", "FTR", "HTHG", "HTAG", "HTR",
         "Attendance", "Referee", "HS", "AS", "HST", "AST", "HHW", "AHW", "HC", "AC", "HF",
         "AF", "HO", "AO", "HY", "AY", "HR", "AR", "HBP", "ABP", "Season"]]


data = DataFrame(data=data)

home_tm = 'Man United'
away_tm = 'Liverpool'
#
# data_h = data.loc[data.HomeTeam == home_tm]
# data_h = data_h.loc[data_h.AwayTeam == away_tm]
#
#
# data_a = data.loc[data.HomeTeam == away_tm]
# data_a = data_a.loc[data_a.AwayTeam == home_tm]


data_hh = data.loc[data.HomeTeam == home_tm]
# data_ah = data.loc[data.HomeTeam == away_tm]
#
# data_ha = data.loc[data.AwayTeam == away_tm]
data_aa = data.loc[data.AwayTeam == home_tm]
#
# data = DataFrame(pd.concat([data_hh, data_ah, data_ha, data_aa]))
data = DataFrame(pd.concat([data_hh, data_aa]))

# data = DataFrame(pd.concat([data_ah, data_ha]))

data = data.sort_index()
# data = data.sort_index()
print(data.tail())


result = {"H": 1, "D": 2, "A": 3}
inverse_result = {val: key for key, val in result.items()}

data["FTR"] = data["FTR"].map(result)

data["HTR"] = data["HTR"].map(result)

# data = data.dropna(thresh=2)
home = data["HomeTeam"].values
class_le.fit(home)
print(np.unique(data.HomeTeam.values))
data["HomeTeam"] = class_le.transform(home)
data["AwayTeam"] = class_le.transform(y=data["AwayTeam"].values)

data.fillna(value=-1, inplace=True)
Y = data.FTR
X = data.drop(['FTR', 'Div', 'Referee', 'Attendance'], axis=1)
# X = X.drop(, axis=1)
# X = X.drop( axis=1)
# X = X.drop(, axis=1)
print(X.columns)
X = X[1:]
Y = Y[:-1]

stdsc = StandardScaler()

train_size = 0.8
train = int(len(X) * train_size)

x_train = X[:train]
x_train_std = stdsc.fit_transform(X=x_train)

x_test = X[train:]
x_test_std = stdsc.transform(X=x_test)

y_train = Y[:train]
y_test = Y[train:]

lr_model = LogisticRegression()

k_features = 22
sbs = SequentialBackwardSelection(lr_model, k_features=1)
sbs.fit(X=x_train_std, y=y_train)
k_feat = [len(k) for k in sbs.subsets_]
plt.plot(k_feat, sbs.scores_, marker='o')
plt.ylim([0.0, 1.1])
plt.ylabel('Accuracy')
plt.xlabel('Number of features')
plt.grid()
plt.show()

k5 = list(sbs.subsets_[6])
print("k5 length: {}".format(len(k5)))
print("k5 is {}".format(k5))
print(data.columns[1:][k5])

lr_model.fit(x_train_std, y_train)
print("without SBS")
print("Training accuracy {}".format(lr_model.score(x_train_std, y_train)))
print("Test accuracy {}".format(lr_model.score(x_test_std, y_test)))

lr_model.fit(x_train_std[:, k5], y_train)
print("with SBS")
print("the five features {}".format(k5))
print("Training accuracy {}".format(lr_model.score(x_train_std[:, k5], y_train)))
print("Test accuracy {}".format(lr_model.score(x_test_std[:, k5], y_test)))


y_train_pred = lr_model.predict(x_train_std[:, k5])
y_test_pred = lr_model.predict(x_test_std[:, k5])


confmat = confusion_matrix(y_test, y_test_pred)
print(confmat)
fig, ax = plt.subplots(figsize=(2.5, 2.5))
ax.matshow(confmat, cmap=plt.cm.Blues, alpha=0.3)
for i in range(confmat.shape[0]):
    for j in range(confmat.shape[1]):
        ax.text(x=j, y=i, s=confmat[i, j], va='center', ha='center')

plt.xlabel('predicted label')
plt.ylabel('true label')
plt.show()
