import os

import pandas as pd
import time


class PullData(object):

    def __init__(self):
        self.football_data = None
        self.download_football_data()

    def download_football_data(self):
        """

        :rtype: object
        """
        pieces = []
        for i in range(1, 17):
            year = str(i).zfill(2) + str(i + 1).zfill(2)
            print("Year {}".format(year))
            data = 'http://football-data.co.uk/mmz4281/' + year + '/E0.csv'
            dd = pd.read_csv(data, error_bad_lines=False, parse_dates=['Date'])
            dd['Season'] = year
            pieces.append(dd)
            time.sleep(2)
        # dd.to_csv(save_file)
        merge = pd.concat(pieces, ignore_index=True)
        filename = 'data/full.csv'

        merge.to_csv(filename, index=False)

        self.football_data = merge
        return self

if __name__ == '__main__':
    dr = PullData()
    print(dr.football_data)
