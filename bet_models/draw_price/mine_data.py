from time import sleep

import numpy as np
import pandas as pd

from bet_models.Utils.utils import (odd_price, day_of_the_week)
from bet_models.draw_price.mine_driver import NavigateTheWeb

nav = NavigateTheWeb()
tomorrow = day_of_the_week()
url = "https://www.skybet.com/football/ev_time/all?dp=" + tomorrow
nav.driver.get(url=url)
sleep(3)

games = nav.get_elements(value='.outcome .oc-desc')
odds = nav.get_elements(value='.outcome .odds')

games_and_odds = []
for game, odd in zip(games, odds):
    games_and_odds.append([game.text, odd_price(odd.text)])

games_and_odds = np.concatenate(games_and_odds)

table = np.array(games_and_odds)
col = 6
row = int(len(games_and_odds)/col)
table = table.reshape(row, col)
print(table)
df_table = pd.DataFrame(data=table)
filename = 'data/' + tomorrow + '.csv'
df_table.to_csv(filename, index=False, header=None)
sleep(5)
nav.driver.quit()


