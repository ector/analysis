from selenium import webdriver
from selenium.webdriver.common.by import By


class NavigateTheWeb(object):
    """
    Use with selenium to get data from the betting web
    """
    def __init__(self, retry_sec=3):
        self.driver = webdriver.Chrome()
        self.retry_sec = retry_sec

    def get_elements(self, by='css', value=None):
        if by is 'xpath':
            elems = self.driver.find_elements(by=By.XPATH, value=value)
        elif by is 'id':
            elems = self.driver.find_elements(by=By.ID, value=value)
        elif by is 'text':
            elems = self.driver.find_elements(by=By.LINK_TEXT, value=value)
        else:
            elems = self.driver.find_elements(by=By.CSS_SELECTOR, value=value)

        return elems

    def get_element(self, by='css', value=None):
        if by is 'xpath':
            elem = self.driver.find_element(by=By.XPATH, value=value)
        elif by is 'id':
            elem = self.driver.find_element(by=By.ID, value=value)
        elif by is 'text':
            elem = self.driver.find_element(by=By.LINK_TEXT, value=value)
        else:
            elem = self.driver.find_element(by=By.CSS_SELECTOR, value=value)

        return elem
