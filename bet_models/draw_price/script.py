import pandas as pd
import numpy as np

from bet_models.Utils.utils import day_of_the_week

tomorrow = day_of_the_week()
price_for_each = 2.00
df = pd.read_csv('../data/'+tomorrow+'.csv', header=None)
print(df.head())
df_wod = df.drop(df[[2, 3]], axis=1)
df_wod.columns = ["home", "h", "away", "a"]
df_wod = df_wod[["home", "away", "h", "a"]]

# percentage of each price
df_pct = 1. / df_wod[["h", "a"]].values

sum_pct = np.sum(df_pct, axis=1)

profit = price_for_each * ((1 / sum_pct) - 1)

df_stakes = (price_for_each * df_pct) / sum_pct[:, None]

print("Total stake: £%.2f" % (price_for_each * len(profit)))
print("Maximum take home: £%.2f" % profit.sum(axis=0))

df_pct_sum = np.append(df_pct, sum_pct[:, None], axis=1)
df_pct_sum_profit = np.append(df_pct_sum, profit[:, None], axis=1)
df_semi = np.append(df_pct_sum_profit, df_stakes, axis=1)
df_prod = pd.DataFrame(data=df_semi.round(2), columns=["HP", "AP", "SHAP", "P", "HS", "AS"])
df_final = pd.DataFrame(pd.concat([df_wod, df_prod], axis=1))
df_final.to_csv("../data/"+tomorrow+"_analysis.csv", index=False)
print(df_final.head(10))
print("price on 10 matches: £{}".format(df_final.P.head(10).sum()))
