from datetime import date
import calendar

weekday = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']


def convert_str_fraction_to_float(s):
    """
    :param s: fraction in string
    :return: float to 2 d.p
    """
    num, denom = s.split('/')
    decimal = float(num) / float(denom)
    dp_2 = round(decimal, 2)
    return dp_2


def day_of_the_week():
    """
    Get today's date to work out what day tomorrow is
    :return: string
    """
    my_date = str(date.today())
    year, month, day = my_date.split('-')
    today = calendar.weekday(year=int(year), month=int(month), day=int(day))
    tomorrow = today + 1

    if tomorrow >= 7:
        tomorrow = 0

    return weekday[tomorrow]


def odd_price(s):
    """
    :param s: fraction in string
    :return: float odd to 2 d.p
    """
    odd = 1 + convert_str_fraction_to_float(s)
    return odd

if __name__ == '__main__':
    print(convert_str_fraction_to_float("3/7"))
    # print(day_of_the_week())
